/**
 * Created by ashish on 1/2/17.
 */
app.controller('dashBoardController', ['$scope', '$interval', 'api','mysqlData','mongoData', dashBoardController]);

function dashBoardController($scope, $interval, api,mysqlData,mongoData) {
    $scope.selectedInterval = 5000;
    $scope.Timer = null;
    $scope.labels = mysqlData.labels;
    $scope.series = mysqlData.series;
    $scope.data = mysqlData.data;

    $scope.mongoLabels = mongoData.labels;
    $scope.mongoSeries = mongoData.series;
    $scope.mongoData = mongoData.data;

    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };


    //Timer start function.
    $scope.StartTimer = function () {
        //Set the Timer start message.
        $scope.Message = "Timer started. ";

        //Initialize the Timer to run every 1000 milliseconds i.e. one second.
        $scope.Timer = $interval(function () {

            api.executeGet('/getMysqlData', {}, function (response) {
            console.log(response);
                $scope.series =   _.keys(_.countBy(response.data.data, "series_name"));
                $scope.labels =  _.uniq(_.pluck(response.data.data, "label"))
                $scope.data = [
                    randomArray(7, 100),
                    randomArray(7, 100)
                ];
            }, function (response) {

            });

            api.executeGet('/getData', {}, function (response) {

                 data = {
                    series: _.pluck(response.data.data,'series'),
                    labels:_.pluck(response.data.data,'labels'),
                    data:[
                        randomArray(7, 100),
                        randomArray(7, 100)
                    ]
                }

                $scope.mongoLabels = data.labels;
                $scope.mongoSeries = data.series;
                $scope.mongoData = data.data;

            }, function (response) {

            });

        }, parseInt($scope.selectedInterval));
    };

    //Timer stop function.
    $scope.StopTimer = function () {

        //Set the Timer stop message.
        $scope.Message = "Timer stopped.";

        //Cancel the Timer.
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };

    $scope.StartTimer();

    $scope.changeselectedInterval = function (interval) {
        $scope.StopTimer();
        $scope.selectedInterval = interval;
        $scope.StartTimer();

    }

    function randomArray(length, max) {
        return Array.apply(null, Array(length)).map(function (_, i) {
            return Math.round(Math.random() * max);
        });
    }


    // Simulate async data update


}