/**
 * Created by ashish on 1/2/17.
 */
var app = angular.module('myApp', [
    'ui.router',
    'chart.js'
]).config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        chartColors: ['#FF5252', '#FF8A80'],
        responsive: false
    });
    // Configure all line charts
    ChartJsProvider.setOptions('line', {
        showLines: false
    });
}]);

/**
 * Created by ashish on 20/12/16.
 */
app.config(function ($stateProvider, $urlRouterProvider) {


    var qcs = {
        name: 'qcs',
        url: '/',
        templateUrl: "/javascripts/partials/dashboard.html",
        controller: 'dashBoardController',
        resolve:{


            mysqlData:function(api){
                function randomArray(length, max) {
                    return Array.apply(null, Array(length)).map(function (_, i) {
                        return Math.round(Math.random() * max);
                    });
                }
                return api.executeGet('/getMysqlData', {}, function (response) {
                    console.log(response);

                    return data = {
                        series: _.keys(_.countBy(response.data.data, "series_name")),
                        labels:_.uniq(_.pluck(response.data.data, "label")),
                        data:[
                            randomArray(7, 100),
                            randomArray(7, 100)
                        ]
                    }
                }, function (response) {

                })
            },
            mongoData:function(api){

                function randomArray(length, max) {
                    return Array.apply(null, Array(length)).map(function (_, i) {
                        return Math.round(Math.random() * max);
                    });
                }
                return api.executeGet('/getData', {}, function (response) {

                    return data = {
                        series: _.pluck(response.data.data,'series'),
                        labels:_.pluck(response.data.data,'labels'),
                        data:[
                            randomArray(7, 100),
                            randomArray(7, 100)
                        ]
                    }
                }, function (response) {

                })
            }
        }

    };


    $stateProvider.state(qcs)
    $urlRouterProvider.otherwise("/#/");
});


app.service('api', ['$http', api]);
function api($http) {

    this.executeGet = function (url, data, success, failure) {

        return $http({
            method: 'GET',
            url: url,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            },
            params: data,
            withCredentials: true
        }).then(success,
            failure);

    };


};