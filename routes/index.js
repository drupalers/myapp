var express = require('express');
var router = express.Router();
var Data = require('../model/data');

var mysql = require('mysql')
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database:'myApp'
})

connection.connect(function(err) {
    if (err) throw err
    console.log('You are now connected...')
})


router.get('/getMysqlData', function (req, res, next) {
    connection.query('SELECT series_name,label,data FROM series, series_label , data  where series.id = series_label.series_id and  series_label.series_id = data.series_id', function (err, series, fields) {
        if (err) throw err

        return res.json({data: series});
    })
});
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/getData', function (req, res, next) {
     Data.find({}, function (err, users) {
        return res.json({data: users});
    });
});

module.exports = router;
