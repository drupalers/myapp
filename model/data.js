var mongoose = require('mongoose');

var dataSchema = mongoose.Schema({
    series:String,
    labels:[String],
    data:[Number],
});

module.exports = mongoose.model('Data', dataSchema);